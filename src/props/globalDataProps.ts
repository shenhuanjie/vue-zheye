import UserProps from '@/props/userProps'
import ColumnProps from '@/props/columnProps'
import PostProps from '@/props/PostProps'

export default interface GlobalDataProps {
  columns: ColumnProps[]
  posts: PostProps[]
  user: UserProps
}
