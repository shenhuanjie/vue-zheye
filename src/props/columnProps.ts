export default interface ColumnProps {
  id: number
  title: string
  avatar?: string
  description: string
}
