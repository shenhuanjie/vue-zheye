export default interface RuleProps {
  type: 'required' | 'email' | 'password'
  message: string
}
