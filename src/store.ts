import { createStore } from 'vuex'
import GlobalDataProps from '@/props/globalDataProps'
import { testData, testPosts } from '@/testData'

const store = createStore<GlobalDataProps>({
  state: {
    columns: testData,
    posts: testPosts,
    user: { isLogin: false }
  },
  mutations: {
    login(state) {
      state.user = { ...state.user, isLogin: true, name: 'viking' }
    }
  },
  getters: {
    biggerColumnLen(state) {
      return state.columns.filter((c) => c.id > 2).length
    },
    getColumnById: (state) => (id: number) => {
      return state.columns.find((c) => {
        if (!c.avatar) {
          c.avatar = require('@/assets/default.png')
        }
        return c.id === id
      })
    },
    getPostsByCid: (state) => (cid: number) => {
      return state.posts.filter((post) => post.columnId === cid)
    }
  }
})

export default store
